const express = require('express');
const userRoutes = require('./server/user/user.route');
const authRoutes = require('./server/auth/auth.route');
const customerRoutes = require('./server/customer/customer.route');
const orderRoutes = require('./server/order/order.route');

const router = express.Router(); // eslint-disable-line new-cap

// TODO: use glob to match *.route files

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

// mount user routes at /users
router.use('/users', userRoutes);
router.use('/customer', customerRoutes);
router.use('/order', orderRoutes);

// mount auth routes at /auth
router.use('/auth', authRoutes);


/****
 *  User Type Route
 */

const UserType = require('./server/models/user-type.model');
router.use('/user-type', require("./server/crud/crud")(UserType));
module.exports = router;
