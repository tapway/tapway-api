const Joi = require('joi');

module.exports = {
  // POST /api/users
  createUser: {
    body: {
      username: Joi.string().required(),
      mobileNumber: Joi.number().required()
    }
  },

  customer_profile: {
    body: {
      fullName: Joi.string().required(),
      homeAddress: Joi.string().required(),
      mobileNumber: Joi.number().required()
    }
  },
  make_order: {
    body: {
      itemName: Joi.string().required(),
      quantity: Joi.number().required(),
      pickup: {
        address: Joi.string().required(),
        contact: Joi.string().required(),
        number: Joi.string().required(),
        state_id: Joi.string().required(),
      },
      delivery: {
        address: Joi.string().required(),
        contact: Joi.string().required(),
        number: Joi.string().required(),
        state_id: Joi.string().required(),
      }
    }
  },

  validate: {
    body: {
      fullName: Joi.string().required(),
      activation_code: Joi.string().required()
    }
  },

  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      username: Joi.string().required(),
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{9}$/).required()
    },
    params: {
      userId: Joi.string().hex().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      username: Joi.string().required(),
      password: Joi.string().required()
    }
  }
};
