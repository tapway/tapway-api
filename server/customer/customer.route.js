const express = require('express');
const validate = require('express-validation');
const paramValidation = require('../../config/param-validation');
const customerController = require('./customer.controller');
const checkAuth = require('../middleware/check-auth');
const router = express.Router(); // eslint-disable-line new-cap


router.route('/profile')
  .post(validate(paramValidation.customer_profile), checkAuth, customerController.profile);


router.route('/profile')
  .get(checkAuth, customerController.myProfile);


module.exports = router;
