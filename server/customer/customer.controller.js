const User = require('../models/user.model');
const Customer = require('../models/customer.model');
const mongoose = require('mongoose');


function profile(req, res, next) {
  console.log(req.body)
  console.log(req.userId)

  const changedEntry = req.body;
  Customer.update({userID: req.userId}, {$set: changedEntry}, {upsert: true}, (e, doc) => {
    if (e)
      res.sendStatus(500);
    else
      return res.status(200).json({
        message: "Edit Successful",
      });
  });
}


function myProfile(req, res, next) {
  const {userId} = req;

  Customer.findOne({
    userID: userId
  }).then(users => {
    return res.status(200).json({
      message: "User Profile",
      data: users,
    });
  })
    .catch(e => res.status(500).send(e));

}


module.exports = {profile, myProfile};
