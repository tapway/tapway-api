const errors = require('@feathersjs/errors');

module.exports = function jsonValidator(validator, schema) {
    return function(hook) {
        let result = validator.validate(hook.data, schema)

        if (result.valid) {
            return hook;
        }

        let error = new Error(schema.message || 'Validation failed')

        error.errors = result.errors.map(error => {
            return {
                path: error.property,
                value: hook.data[error.property],
                message: `${error.property} ${error.message}`
            }
        })

        throw new errors.BadRequest(error, hook.data);
    }
};