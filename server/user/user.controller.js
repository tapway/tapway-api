const User = require('../models/user.model');
const mongoose = require('mongoose');

/**
 * Load user and append to req.
 */
function load(req, res, next, id) {
  User.get(id)
    .then((user) => {
      req.user = user; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

/**
 * Get user
 * @returns {User}
 */
function get(req, res) {
  return res.json(req.user);
}

/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function create(req, res, next) {
  const newEntry = req.body;
  newEntry._id = new mongoose.Types.ObjectId();
  newEntry.activation_code = Math.random().toString(36).substr(2, 11);

  User.create(newEntry, (e, ent) => {
    if (e) {
      if (e.name === 'ValidationError') return validator(e, res) // here
      return res.status(500).json({message: 'Error while creating new object'})
      res.sendStatus(500);
    } else {
      res.send(ent);
    }
  });
}


function validate(req, res, next) {
  const username = req.body.username;
  const activation_code = req.body.activation_code;


  User.findOneAndUpdate({
    username: username,
    activation_code: activation_code,
  }, {
    $set: {
      activation_code: "",
      active: true,
    }
  }, {new: true}, (err, doc) => {
    if (err) {
      console.log(err);
    }
    if (doc === null) {
      return res.status(500).json({message: 'User Not Found'})
    }

    return res.status(200).json({
      message: "Activation Successful",
      data: doc,
    });
  });

}


/**
 * Update existing user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function update(req, res, next) {
  const changedEntry = req.body;
  User.update({_id: req.params._id}, {$set: changedEntry}, (e) => {
    if (e)
      res.sendStatus(500);
    else
      res.sendStatus(200);
  });
}

/**
 * Get user list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {User[]}
 */
function list(req, res, next) {
  const {limit = 50, skip = 0} = req.query;
  User.list({limit, skip})
    .then(users => res.json(users))
    .catch(e => next(e));
}

/**
 * Delete user.
 * @returns {User}
 */
function remove(req, res, next) {
  const user = req.user;
  user.remove()
    .then(deletedUser => res.json(deletedUser))
    .catch(e => next(e));
}

module.exports = {load, get, create, update, list, remove, validate};
