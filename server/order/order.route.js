const express = require('express');
const validate = require('express-validation');
const paramValidation = require('../../config/param-validation');
const orderController = require('./order.controller');
const checkAuth = require('../middleware/check-auth');
const router = express.Router(); // eslint-disable-line new-cap


router.route('/make')
  .post(validate(paramValidation.make_order), checkAuth, orderController.make_order);


router.route('/unaccepted')
  .get(checkAuth, orderController.list);

router.route('/mine/delivered')
  .get(checkAuth, orderController.mine_delivered);

router.route('/mine/cancel/:orderId')
  .get(checkAuth, orderController.cancel);

router.route('/mine/undelivered')
  .get(checkAuth, orderController.mine_undelivered);


router.route('/driver/accept/:orderId')
  .get(checkAuth, orderController.driver_accept);

router.route('/driver/mine/delivered')
  .get(checkAuth, orderController.driver_delivered);

router.route('driver/mine/undelivered')
  .get(checkAuth, orderController.driver_undelivered);


module.exports = router;
