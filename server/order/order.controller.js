const User = require('../models/user.model');
const Order = require('../models/order.model');
const Pickup = require('../models/order-pickup.model');
const Delivery = require('../models/order-delivery.model');
const mongoose = require('mongoose');
const validator = require('../utils/format-validator');

function make_order(req, res, next) {
  const {userId} = req;

  // console.log(req.body);


  const orderEntry = {};

  orderEntry._id = new mongoose.Types.ObjectId();
  orderEntry.itemName = req.body.itemName;
  orderEntry.quantity = req.body.quantity;
  orderEntry.userId = userId;


  let orderPickup = {};
  orderPickup = req.body.pickup;
  orderPickup._id = new mongoose.Types.ObjectId();


  let orderDelivery = {};
  orderDelivery = req.body.delivery;
  orderDelivery._id = new mongoose.Types.ObjectId();


  Order.create(orderEntry, (e, ent) => {
    if (e) {
      if (e.name === 'ValidationError') return validator(e, res);
      return res.status(500).json({message: 'Error while creating new object'});
    }
    orderPickup.orderId = orderEntry._id;
    orderDelivery.orderId = orderEntry._id;
    Pickup.create(orderPickup);
    Delivery.create(orderDelivery);

    const response = {};
    response.order = ent;
    response.pickup = orderPickup;
    response.delivery = orderDelivery;

    return res.status(200).json({
      message: 'Order Created',
      data: response,
    });
  });
}


function list(req, res, next) {
  const userID = req.userId;
  const currentPage = req.query.page || 1;
  const perPage = 10;
  let totalItems;
  const userMap = [];


  Order.find({
    riderId: null,
    isDelivered: false,
    isCanceled: false,
  }).populate(['delivery', 'pickup']).exec((err, users) => {
    if (err) {
      if (err.name === 'ValidationError') return validator(err, res);
      return res.status(500).json({message: 'Error while creating new object'});
    }
    return res.status(200).json({
      message: 'Order List',
      data: users,
    });
  });
}


function driver_delivered(req, res, next) {
  const userID = req.userId;
  const currentPage = req.query.page || 1;
  const perPage = 10;
  let totalItems;
  const userMap = [];


  Order.find({
    riderId: userID,
    isDelivered: true,
  }).populate(['delivery', 'pickup']).exec((err, users) => {
    if (err) {
      if (err.name === 'ValidationError') return validator(err, res);
      return res.status(500).json({message: 'Error while creating new object'});
    }
    return res.status(200).json({
      message: 'My Delivered List',
      data: users,
    });
  });
}

function mine_delivered(req, res, next) {
  const userID = req.userId;
  const currentPage = req.query.page || 1;
  const perPage = 10;
  let totalItems;
  const userMap = [];


  Order.find({
    riderId: {
      $nin: null
    },
    userId: userID,
    isDelivered: true,
  }).populate(['delivery', 'pickup']).exec((err, users) => {
    if (err) {
      if (err.name === 'ValidationError') return validator(err, res);
      return res.status(500).json({message: 'Error while creating new object'});
    }
    return res.status(200).json({
      message: 'My Delivered List',
      data: users,
    });
  });
}

function mine_undelivered(req, res, next) {
  const userID = req.userId;
  const currentPage = req.query.page || 1;
  const perPage = 10;
  let totalItems;
  const userMap = [];


  Order.find({
    riderId: {
      $nin: null
    },
    userId: userID,
    isDelivered: false,
  }).populate(['delivery', 'pickup']).exec((err, users) => {
    if (err) {
      if (err.name === 'ValidationError') return validator(err, res);
      return res.status(500).json({message: 'Error while creating new object'});
    }
    return res.status(200).json({
      message: 'My Delivered List',
      data: users,
    });
  });
}

function driver_undelivered(req, res, next) {
  const userID = req.userId;
  const currentPage = req.query.page || 1;
  const perPage = 10;
  let totalItems;
  const userMap = [];


  Order.find({
    riderId: userID,
    isDelivered: false,
  }).populate(['delivery', 'pickup']).exec((err, users) => {
    if (err) {
      if (err.name === 'ValidationError') return validator(err, res);
      return res.status(500).json({message: 'Error while creating new object'});
    }
    return res.status(200).json({
      message: 'My Delivered List',
      data: users,
    });
  });
}


function driver_accept(req, res, next) {
  const orderId = req.params.orderId;
  const userID = req.userId;
  const currentPage = req.query.page || 1;
  const perPage = 10;
  let totalItems;
  const userMap = [];


  Order.find({
    riderId: {$exists: false},
    _id: orderId,
    isDelivered: false,
  }).exec((err, users) => {
    if (err) {
      if (err.name === 'ValidationError') return validator(err, res);
      return res.status(500).json({message: 'Error while accepting, you cannot accept this order'});
    }

    // console.log(users.length);

    if (users.length >= 1) {
      const changedEntry = {
        riderId: userID
      };

      Order.findByIdAndUpdate(orderId, changedEntry, {new: true}, (err, model) => {
        if (err) {
          return res.status(500).json({message: 'Error while accepting'});
        }
        return res.status(200).json({
          message: 'Edit Successful',
          data: model,
        });
      });
    } else {
      return res.status(500).json({message: 'Error while accepting, you cannot accept this order'});
    }
  });
}


function cancel(req, res, next) {
  const orderId = req.params.orderId;
  const userID = req.userId;
  const currentPage = req.query.page || 1;
  const perPage = 10;
  let totalItems;
  const userMap = [];


  Order.find({
    riderId: {$exists: false},
    _id: orderId,
    isDelivered: false,
    isCanceled: false,
  }).exec((err, users) => {
    if (err) {
      if (err.name === 'ValidationError') return validator(err, res);
      return res.status(500).json({message: 'Error while canceling, you cannot cancel this order'});
    }

    // console.log(users.length);

    if (users.length >= 1) {
      const changedEntry = {
        isCanceled: true
      };

      Order.findByIdAndUpdate(orderId, changedEntry, {new: true}, (err, model) => {
        if (err) {
          return res.status(500).json({message: 'Error while accepting'});
        }
        return res.status(200).json({
          message: 'Cancel order Successful',
          data: model,
        });
      });
    } else {
      return res.status(500).json({message: 'Error while canceling, you cannot cancel this order'});
    }
  });
}


module.exports = {make_order, list, mine_delivered, mine_undelivered, driver_accept, cancel, driver_delivered, driver_undelivered};
