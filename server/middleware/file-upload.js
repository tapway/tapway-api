const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

aws.config.update({
  secretAccessKey: 'GxDb/rZwPXjMwzA1CTwlp3LzQUOJHIIx6UUXViVK',
  accessKeyId: 'AKIAJFZFMFIBI53OPF3A',
  region: 'us-west-2'
});

const s3 = new aws.S3();

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'application/pdf' || file.mimetype === 'application/msword' || file.mimetype === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
    cb(null, true);
  } else {
    cb(new Error('Invalid file type, only JPEG, PNG, PDF, DOC and DOCX are allowed!'), false);
  }
}

const upload = multer({
  storage: multerS3({
    acl: 'public-read',
    s3,
    bucket: 'doctoorav2-doctors-document',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: function (req, file, cb) {
      cb(null, {
        fieldName: file.fieldname
      });
    },
    key: function (req, file, cb) {
      cb(null, Date.now().toString())
    }
  })
})


module.exports = upload;