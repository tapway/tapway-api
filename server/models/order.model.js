const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * User Type Schema
 */
const OrderSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    itemName: {
      type: String,
      required: true
    },
    quantity: {
      type: Number,
      required: true
    },
    isDelivered: {
      type: Boolean,
      default: false
    },
    isCanceled: {
      type: Boolean,
      default: false
    },
    riderId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: false
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: false
    },
    deliveryDate: {
      type: Date,
      required: false
    },
  },
  {
    timestamps: true
  });

OrderSchema.virtual('delivery', {
  ref: 'OrderDelivery', // The model to use
  localField: '_id', // Find people where `localField`
  foreignField: 'orderId', // is equal to `foreignField`
  justOne: true,
  options: {sort: {name: -1}, limit: 5} // Query options, see http://bit.ly/mongoose-query-options
});

OrderSchema.virtual('pickup', {
  ref: 'OrderPickUp', // The model to use
  localField: '_id', // Find people where `localField`
  foreignField: 'orderId', // is equal to `foreignField`
  justOne: true,
  options: {sort: {name: -1}, limit: 5} // Query options, see http://bit.ly/mongoose-query-options
});

OrderSchema.set('toObject', {virtuals: true});
OrderSchema.set('toJSON', {virtuals: true});

/**
 * @typedef User Type
 */
module.exports = mongoose.model('Order', OrderSchema);
