const Promise = require('bluebird');
const mongoose = require('mongoose');
const httpStatus = require('http-status');
const Schema = mongoose.Schema;
const APIError = require('../helpers/APIError');
const mongoosePaginate = require('mongoose-paginate-v2');
/**
 * User Schema
 */
const CustomerSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  fullName: {
    type: String,
    required: true,
  },
  mobileNumber: {
    type: Number,
    required: true,
  },
  homeAddress: {
    type: String,
    required: false,
  },
  logoUrl: {
    type: String,
    required: false,
  },
  stateID: {
    type: Schema.Types.ObjectId,
    ref: 'State',
    required: false
  },
  userID: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },


}, {
  timestamps: true
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */


CustomerSchema.pre('save', function (next) {
  var user = this;
});

/**
 * Methods
 */

CustomerSchema.methods.toJSON = function () {
  var obj = this.toObject();
  delete obj.password;
  return obj;
}


/**
 * Statics
 */
CustomerSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({skip = 0, limit = 50} = {}) {
    return this.find()
      .sort({createdAt: -1})
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};
CustomerSchema.plugin(mongoosePaginate);
/**
 * @typedef User
 */
module.exports = mongoose.model('Customer', CustomerSchema);
