const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * User Type Schema
 */
const OrderDeliverySchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  orderId: {
    type: Schema.Types.ObjectId,
    ref: 'Order',
    required: true
  },
  address: {
    type: String,
    required: true
  },
  contact: {
    type: String,
    required: false
  },
  number: {
    type: Number,
    required: false
  },
  email: {
    type: String,
    required: false
  },
  state_id: {
    type: Number,
    required: true
  }
},
  {
    timestamps: true
  });


/**
 * @typedef User Type
 */
module.exports = mongoose.model('OrderDelivery', OrderDeliverySchema);
