const mongoose = require('mongoose');

/**
 * User Type Schema
 */
const UserTypeSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
},
  {
    timestamps: true
  });


/**
 * @typedef User Type
 */
module.exports = mongoose.model('UserType', UserTypeSchema);
