const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * User Type Schema
 */
const OrderPickUpSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  orderId: {
    type: Schema.Types.ObjectId,
    ref: 'Order',
    required: true
  },
  address: {
    type: String,
    required: true
  },
  contact: {
    type: String,
    required: true
  },
  number: {
    type: Number,
    required: true
  },
  email: {
    type: String,
    required: false
  },
  state_id: {
    type: Number,
    required: true
  }
},
  {
    timestamps: true
  });


/**
 * @typedef User Type
 */
module.exports = mongoose.model('OrderPickUp', OrderPickUpSchema);
