const Promise = require('bluebird');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const httpStatus = require('http-status');
const APIError = require('../helpers/APIError');
const bcrypt = require('bcrypt');
const mongoosePaginate = require('mongoose-paginate-v2');
SALT_WORK_FACTOR = 10;
/**
 * User Schema
 */
const UserSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  username: {
    type: String,
    required: true,
    unique: true,
  },
  userType: {
    type: Schema.Types.ObjectId,
    ref: 'UserType',
    required: true
  },
  password: {
    type: String,
    required: true,
  },
  active: {
    type: Boolean,
    required: false,
    default: false
  },
  activation_code: {
    type: String,
    required: false,
  },
  mobileNumber: {
    type: Number,
    required: true,
  },
}, {
  timestamps: true
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

UserSchema
  .virtual('pass')
  // set methods
  .set(function (password) {
    this._password = password;
  });


UserSchema.pre('save', function (next) {
  var user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      user.password = hash;
      next();
    });
  });
});

/**
 * Methods
 */

UserSchema.methods.toJSON = function () {
  var obj = this.toObject();
  delete obj.password;
  return obj;
}

UserSchema.methods.validatePassword = function validatePassword(data) {
  console.log(this.username)
  return bcrypt.compareSync(data, this.password);
};

UserSchema.methods.comparePassword = function (candidatePassword, cb) {
  var user = this;
  bcrypt.compare(candidatePassword, user.password, function (err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

UserSchema.methods.isPasswordValid = function (password) {
  return bcrypt.compareSync(password, this.password);
};

/**
 * Statics
 */
UserSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({skip = 0, limit = 50} = {}) {
    return this.find()
      .sort({createdAt: -1})
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};
UserSchema.plugin(mongoosePaginate);
/**
 * @typedef User
 */
module.exports = mongoose.model('User', UserSchema);
