const jwt = require('jsonwebtoken');
const httpStatus = require('http-status');
const APIError = require('../helpers/APIError');
const config = require('../../config/config');
const User = require('../models/user.model');
const bcrypt = require('bcrypt');

// sample user, used for authentication
const user = {
  username: 'react',
  password: 'express'
};

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function login(req, res, next) {
  // Ideally you'll fetch this from the db
  // Idea here was to show how jwt works with simplicity


  const data = req.body;
  let UserData, UserDetails;


  User.findOne({
    username: data.username
  })
    .then(user => {
      if (!user) {
        const error = new Error('An User with this username could not be found.');
        error.statusCode = 801;
        throw error;
      }
      UserData = user;
      UserDetails = user.toJSON();
      return bcrypt.compare(data.password, user.password);
    })
    .then(isEqual => {
      if (!isEqual) {
        const error = new Error('Wrong password!');
        error.statusCode = 901;
        throw error;
      }
      const token = jwt.sign({
          username: UserData.username,
          userId: UserData._id.toString()
        },
        'somesupersecretsecret', {
          expiresIn: '30d'
        }
      );
      res.status(200).json({
        message: "Auth successful, you are now logged",
        token: token,
        UserDetails
      });
    })
    .catch(err => {
      console.log(err);
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });

}

/**
 * This is a protected route. Will return random number only if jwt token is provided in header.
 * @param req
 * @param res
 * @returns {*}
 */
function getRandomNumber(req, res) {
  // req.user is assigned by jwt middleware if valid token is provided
  return res.json({
    user: req.user,
    num: Math.random() * 100
  });
}

module.exports = {login, getRandomNumber};
